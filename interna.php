<!doctype html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<title>Imobille Negócios</title>

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/x-icon" href="favicon.png">

		<link rel="stylesheet" href="style.css">
	</head>

	<body>
		<div id="immobile">
			<?php require 'layout/header.php'; ?>

			<div class="container">
				<div class="row">
					<div class="col-md-12 col-lg-8">
						<div class="code">
							CÓDIGO DO IMÓVEL: <strong>AP0797</strong>
						</div>

						<div class="card card-in">
							<div class="card-header">
								<h1 class="title">Apartamento no edifício Vitra Balneário Camboriú</h1>

								<!-- if normal -->
								<!-- <div class="price">R$ 2.650.000</div> -->
								<!-- end if -->

								<!-- if promocao -->
								<div class="promotion">
									<div class="from">
										<div class="label">
											DE:
										</div>

										<div class="price">
											R$ 2.650.000
										</div>
									</div>

									<div class="to">
										<div class="label">
											POR:
										</div>

										<div class="price">
											R$ 2.250.000
										</div>
									</div>
								</div>
								<!-- end if -->
							</div>

							<div class="card-body">
								<ul class="nav nav-tabs">
									<li class="nav-item">
										<a class="nav-link active" data-toggle="tab" href="#photos" role="tab">
											<?php require 'icons/photos.php'; ?>

											Fotos do imóvel
										</a>
									</li>

									<li class="nav-item">
										<a class="nav-link" data-toggle="tab" href="#location" role="tab">
											<?php require 'icons/world.php'; ?>											

											Localização
										</a>
									</li>
								</ul>

								<div class="tab-content">
									<div class="tab-pane fade show active photod-tab" id="photos" role="tabpanel">
										<div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
											<div class="carousel-inner">
												<div class="carousel-item active">
													<img src="assets/home/ibiza.jpg" class="d-block w-100" alt="...">
												</div>

												<div class="carousel-item">
													<img src="assets/home/ibiza.jpg" class="d-block w-100" alt="...">
												</div>

												<div class="carousel-item">
													<img src="assets/home/ibiza.jpg" class="d-block w-100" alt="...">
												</div>
											</div>

											<a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
												<span class="carousel-control-prev-icon" aria-hidden="true"></span>
												<span class="sr-only">Previous</span>
											</a>
											<a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
												<span class="carousel-control-next-icon" aria-hidden="true"></span>
												<span class="sr-only">Next</span>
											</a>
										</div>
									</div>

									<div class="tab-pane fade location-tab" id="location" role="tabpanel">
										<p class="address">
											<?php require 'icons/pin.php'; ?>

											Santa Catarina — Balneário Camboriú — Rua 2300
										</p>

										<div id="map" class="map">
											mapa aqui
										</div>
									</div>
								</div>

								<div class="table">
							    	<div class="container-fluid">
							    		<div class="row">
							    			<div class="col-md-3 cell">
							    				<div class="cell-1">
								    				<div class="cell-label">
								    					dorm.
								    				</div>

								    				<div class="cell-value">
								    					03
								    				</div>
								    			</div>

								    			<div class="cell-2 cell-dormitorio">
								    				
								    			</div>
							    			</div>

							    			<div class="col-md-3 cell">
							    				<div class="cell-1">
								    				<div class="cell-label">
								    					suítes
								    				</div>

								    				<div class="cell-value">
								    					03
								    				</div>
								    			</div>

								    			<div class="cell-2 cell-suite">
								    				
								    			</div>
							    			</div>

							    			<div class="col-md-3 cell">
							    				<div class="cell-1">
								    				<div class="cell-label">
								    					área
								    				</div>

								    				<div class="cell-value">
								    					03
								    				</div>
								    			</div>

								    			<div class="cell-2 cell-area">
								    				
								    			</div>
							    			</div>

							    			<div class="col-md-3 cell">
							    				<div class="cell-1">
								    				<div class="cell-label">
								    					vagas
								    				</div>

								    				<div class="cell-value">
								    					03
								    				</div>
								    			</div>

								    			<div class="cell-2 cell-garagem">
								    				
								    			</div>
							    			</div>
							    		</div>
							    	</div>
							    </div>

							    <div class="collapse" id="collapseImmobile">
								    <p class="card-text">
								    	<strong>DESIGN ITALIANO INOVAÇÃO E ELEGÂNCIA</strong> - Apartamento no edifício Vitra Balneário Camboriú, com 171m² privativos, 04 suítes, sala de estar, sala de jantar, lavabo, cozinha, área de serviço e 03 vagas de garagem. Empreendimento desenhado e projetado by Pininfarina uma das casas de desing mais renomadas do mundo, consta com área de lazer completa e todas as áreas comuns decoradas, localizado em esquina da Avenida Brasil, Centro/sul de Balneário Camboriú.
								    </p>

								    <div class="card-info">
								    	<h3>Características do Apartamento:</h3>

								    	<ul>
										 	<li>04 suítes sendo 01 master com hidro e duas com vista para o mar.</li>
										 	<li>Amplo Living para 03 ambientes</li>
										 	<li>Espaço Gourmet com churrasqueira e exaustor</li>
										 	<li>Cozinha Integrada</li>
										 	<li>Lavabo</li>
										 	<li>Área de Serviço</li>
										 	<li>Área Técnica</li>
										</ul>

										<h3>Acabamento:</h3>

										<ul>
										 	<li>Acabamento em gesso em todos os ambientes</li>
										 	<li>Piso em porcelanato 80 x 80 e laminado de madeira na parte íntima</li>
										 	<li>Infra-estrutura para instalação de ar-condicionado tipo Split</li>
										 	<li>Persianas em esquadrias de alumínio</li>
										 	<li>Biometria (previsão)</li>
										 	<li>Automação de cortinas entregue motorizado</li>
										 	<li>Automação de sonorização (previsão)</li>
										 	<li>Automação de iluminação (previsão)</li>
										 	<li>Aspiração central (pontos e tubulação)</li>
										 	<li>Hidrômetros individuais</li>
										 	<li>Medidor de gás individual</li>
										 	<li>Portas laqueadas</li>
										 	<li>Instalação hidráulica aérea</li>
										 	<li>Pontos de água quente</li>
										</ul>

										<hr>

										<h3>Gostou do imóvel? Envie sua proposta!</h3>

										<hr>

										<p>
											Imóvel disponível para visitação. Agende uma visita agora mesmo e venha conhecer este lindo imóvel Os valores estão sujeitos a alteração sem aviso prévio.
										</p>

										<hr>

										<a href="/">Conheça Outros Lançamentos à Venda em Balneário Camboriú</a>

										<hr>

										<p>
											Não encontrou o que procurava?! <a href="/"><strong>Pare de buscar, clique aqui e encomende!</strong></a>
										</p>
								    </div>
								</div>

								<button class="btn btn-link btn-collapse-toggle" data-toggle="collapse" data-target="#collapseImmobile">
									Continuar lendo...
								</button>

							    <div class="card-actions">
							    	<button class="btn btn-primary">
							    		Quero mais informações
							    	</button>
							    </div>
							</div>
						</div>
					</div>

					<div class="col-md-12 col-lg-4">
						<div class="card-contact" id="cardContact">
							<div class="phone-handler">
								<a href="" class="phone">
									<?php require 'icons/whats.php'; ?>

									(47) 99999-9999
								</a>

								<button class="btn btn-link white btn-show-phone">
									Ver telefone
								</button>
							</div>

							<div class="label">
								Saiba mais sobre este imóvel
							</div>

							<form>
								<input type="text" class="form-control" placeholder="Nome" required>

								<input type="email" class="form-control" placeholder="E-mail" required>

								<input type="text" class="form-control" placeholder="(DDD) Telefone" required>

								<textarea class="form-control">Olá, tenho interesse neste imóvel: Apartamento no edifício Vitra Balneário Camboriú | código #AP0999, preço a partir de R$ 2.650.000. Aguardo o contato. Obrigado.</textarea>

								<button type="submit" class="btn btn-success btn-submit">
									Receber informações
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>

			<section class="related">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 d-flex align-items-center justify-content-center">
							<h2>Imóveis Similares</h2>
						</div>
					</div>

					<div class="row">
						<div class="col-md-3">
							<?php require 'layout/card-vertical.php'; ?>
						</div>

						<div class="col-md-3">
							<?php require 'layout/card-vertical.php'; ?>
						</div>

						<div class="col-md-3">
							<?php require 'layout/card-vertical.php'; ?>
						</div>

						<div class="col-md-3">
							<?php require 'layout/card-vertical.php'; ?>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12 d-flex align-items-center justify-content-center">
							<button class="btn btn-primary btn-two-line btn-related">
								<div class="line-1">Ainda não achou?</div>

								<div class="line-2">Encontramos seu imóvel ideal!</div>
							</button>
						</div>
					</div>
				</div>
			</section>

			<?php require 'layout/order.php'; ?>

			<?php require 'layout/scrollTop.php' ?>

			<?php require 'layout/footer.php'; ?>
		</div>

		<script src="index.js"></script>
		<script src="main.js"></script>
	</body>
</html>