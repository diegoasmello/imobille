<!doctype html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<title>Imobille Negócios | Guia de Estilo</title>

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/x-icon" href="favicon.png">

		<link rel="stylesheet" href="style.css">
	</head>

	<body>
		<div id="styleguide">
			<div class="style-section">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<a class="navbar-brand" href="/">
								<img height="45" src="assets/logo-dark.svg">
							</a>
						</div>

						<div class="col-md-6 justify-content-end align-items-center d-flex">
							<h1 class="style-guide-title d-flex justify-content-end align-items-center mb-0">Guia de estilo</h1>
						</div>
					</div>
				</div>
			</div>

			<div class="style-section">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2 class="style-guide-title">Cores</h2>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<h3 class="">Cores primárias</h3>

							<div class="style-guide-row">
								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 primary-1">
											<div class="value">
												$primary-1
											</div>
										</div>

										<div class="palette palette-2 primary-2">
											<div class="value">
												$primary-2
											</div>
										</div>
									</div>
								</div>

								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 secondary-1">
											<div class="value">
												$secondary-1
											</div>
										</div>

										<div class="palette palette-2 secondary-2">
											<div class="value">
												$secondary-2
											</div>
										</div>
									</div>
								</div>
							</div>

							<h3 class="">Cores de texto</h3>

							<div class="style-guide-row">
								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 text-1">
											<div class="value">
												$text-1
											</div>
										</div>
									</div>
								</div>

								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 text-2">
											<div class="value">
												$text-2
											</div>
										</div>
									</div>
								</div>

								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 text-3">
											<div class="value">
												$text-3
											</div>
										</div>
									</div>
								</div>

								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 text-4">
											<div class="value">
												$text-4
											</div>
										</div>
									</div>
								</div>

								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 text-5">
											<div class="value">
												$text-5
											</div>
										</div>
									</div>
								</div>

								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 text-6">
											<div class="value">
												$text-6
											</div>
										</div>
									</div>
								</div>
							</div>

							<h3 class="">Cores de fio</h3>

							<div class="style-guide-row">
								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 ui-1">
											<div class="value">
												$ui-1
											</div>
										</div>
									</div>
								</div>

								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 ui-2">
											<div class="value">
												$ui-2
											</div>
										</div>
									</div>
								</div>

								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 ui-3">
											<div class="value">
												$ui-3
											</div>
										</div>
									</div>
								</div>
							</div>

							<h3 class="">Tons de cinza</h3>

							<div class="style-guide-row">
								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 gray-1 bordered">
											<div class="value">
												$gray-1
											</div>
										</div>
									</div>
								</div>

								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 gray-2 bordered">
											<div class="value">
												$gray-2
											</div>
										</div>
									</div>
								</div>

								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 gray-3 bordered">
											<div class="value">
												$gray-3
											</div>
										</div>
									</div>
								</div>
							</div>

							<h3 class="">Cores de notificação</h3>

							<div class="style-guide-row">
								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 success-1">
											<div class="value">
												$success-1
											</div>
										</div>

										<div class="palette palette-2 success-2">
											<div class="value">
												$success-2
											</div>
										</div>
									</div>
								</div>

								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 warning-1">
											<div class="value">
												$warning-1
											</div>
										</div>

										<div class="palette palette-2 warning-2">
											<div class="value">
												$warning-2
											</div>
										</div>
									</div>
								</div>

								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 info-1">
											<div class="value">
												$info-1
											</div>
										</div>

										<div class="palette palette-2 info-2">
											<div class="value">
												$info-2
											</div>
										</div>
									</div>
								</div>

								<div class="style-guide-col">
									<div class="style-guide-cell">
										<div class="palette palette-1 danger-1">
											<div class="value">
												$danger-1
											</div>
										</div>

										<div class="palette palette-2 danger-2">
											<div class="value">
												$danger-2
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="style-section">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2 class="style-guide-title">Tipografia</h2>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="style-guide-cell">
								<div class="code">
									h1.display-1 | 42px | Roboto Bold
								</div>

								<h1 class="display-1">Imobille Negócios</h1>
							</div>

							<div class="style-guide-cell">
								<div class="code">
									h1.display-2 | 38px | Roboto Bold
								</div>
								
								<h1 class="display-2">Imobille Negócios</h1>
							</div>

							<div class="style-guide-cell">
								<div class="code">
									h1 | 38px | Roboto
								</div>
								
								<h1>Imobille Negócios</h1>
							</div>

							<div class="style-guide-cell">
								<div class="code">
									h2 | 29px | Poppins
								</div>
								
								<h2>Imobille Negócios</h2>
							</div>

							<div class="style-guide-cell">
								<div class="code">
									h3 | 24px | Roboto Bold
								</div>
								
								<h3>Imobille Negócios</h3>
							</div>

							<div class="style-guide-cell">
								<div class="code">
									h4 | 16px | Roboto Bold
								</div>
								
								<h4>Imobille Negócios</h4>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="style-section">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2 class="style-guide-title">Botões</h2>

							<h3>Botões da home</h3>

							<div class="style-guide-row style-guide-cell home-bg">
								<div class="style-guide-col">
									<button class="btn btn-home btn-primary">Home btn primary</button>
								</div>

								<div class="style-guide-col">
									<button class="btn btn-home btn-secondary">Home btn secondary</button>
								</div>
							</div>

							<h3>Botões gerais</h3>

							<div class="style-guide-cell">
								<div class="style-guide-row style-guide-cell">
									<div class="style-guide-col">
										<button class="btn btn-primary">Btn primary</button>
									</div>

									<div class="style-guide-col">
										<button class="btn btn-secondary">Btn secondary</button>
									</div>

									<div class="style-guide-col">
										<button class="btn btn-success">Btn btn-success</button>
									</div>
									
									<div class="style-guide-col">
										<button class="btn btn-link">Btn link</button>
									</div>

									<div class="style-guide-col black">
										<button class="btn btn-link white">Btn link</button>
									</div>
								</div>
							</div>


							<h3>Botão com duas linhas</h3>

							<div class="style-guide-row style-guide-cell">
								<div class="style-guide-col">
									<button class="btn btn-primary btn-two-line">
										<div class="line-1">Ainda não achou?</div>

										<div class="line-2">Clique aqui, encontramos pra você!</div>
									</button>
								</div>

								<div class="style-guide-col">
									<button class="btn btn-secondary btn-two-line style-guide-col">
										<div class="line-1">Ainda não achou?</div>

										<div class="line-2">Clique aqui, encontramos pra você!</div>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php require 'layout/scrollTop.php' ?>
	</body>
</html>