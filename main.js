$(function(){
	var didScroll;
	var lastScrollTop = 0;
	var delta = 5;
	var navbarHeight = $('#header').outerHeight();
	var windowHeight = $(window).height();

	hasScrolled();

	$(window).scroll(function(event){
	    didScroll = true;
	});

	setInterval(function() {
	    if (didScroll) {
	        hasScrolled();
	        didScroll = false;
	    }
	}, 50);

	function hasScrolled() {
	    var st = $(this).scrollTop();
	    var header = $('#header');
	    var footerTop = $('#footer').offset().top - $('#footer').css("padding-top").replace('px', '');
	    
	    if (Math.abs(lastScrollTop - st) <= delta) {
	        return;
	    }

	    if (st < windowHeight) {
	  		$('.scrollToTop').addClass('hide');
        } else  {
        	$('.scrollToTop').removeClass('hide');
        }

	    if (st < navbarHeight) {
	    	$('.jumbotron-mouse').removeClass('hide');
        	$('#header').removeClass('fixed');
        } else  {
        	$('.jumbotron-mouse').addClass('hide');
            $('#header').addClass('fixed');
        }

        if (st < footerTop) {
        	$('#footer').removeClass('footer-scroll');
        } else  {
            $('#footer').addClass('footer-scroll');
        }

	    lastScrollTop = st;
	}

	$('.card-immobile .carousel-control-prev').click(function(e){
		$(this).parent().carousel('prev');
		e.preventDefault();
	})

	$('.card-immobile .carousel-control-next').click(function(e){
		console.log($(this).parent().carousel('next'));
		e.preventDefault();
	})

	$('.card-immobile .btn-whats').click(function(e){
		window.open($(this).data('href'), '_blank');
		e.preventDefault();
	})

	$('#btnHeaderToggle').click(function(){
		$('#headerNav').toggleClass('show');
	})

	$('#cardContact .btn-show-phone').click(function(){
		$(this).toggleClass('hide');
	})

	$('.scrollTo').click(function(){
		$([document.documentElement, document.body]).animate({
	        scrollTop: $($(this).data('href')).offset().top - 100
	    }, 400);
	})

	$('.scrollToTop').click(function(){
		$([document.documentElement, document.body]).animate({
	        scrollTop: $('body').offset().top
	    }, 300);	
	})

	//
	// funções de filtro da lista
	//

	$('.filters-applied ul li .close').click(function(){
		alert('Função do filtro!');
	})

	$('.filter-main-row select').change(function(){
		alert('Função do filtro!');
	})

	$('.filter-btn').click(function(){
		var btns = $(this).parent('.filter-btn-group').find('.filter-btn');

		btns.removeClass('active');
		$(this).toggleClass('active');
		alert('Função do filtro!');
	})

	$('.filter-item-body .form-check .form-check-input').change(function() {
		alert('Função do filtro!');

	    if (this.checked) {
	       //do stuff 
	    } else {
	    	//do stuff
	    }
	});

	// end filtro

	$('.list-menu .btn-filter-toggle').click(function(){
		if (!$(this).hasClass('active')) {
			openFilter();
		} else {
			closeFilter();
		}
	})

	$('.list-menu .btn-whats').click(function(e){
		window.open($(this).data('href'), '_blank');
		e.preventDefault();
	})

	$('.filter-handler .close').click(function() {
		closeFilter();
	})

	function openFilter(){
		$('.btn-filter-toggle').addClass('active');
		$('.filter-handler').addClass('show');
		$('body').addClass('modal-open');
	}

	function closeFilter(){
		$('.btn-filter-toggle').removeClass('active');
		$('.filter-handler').removeClass('show');
		$('body').removeClass('modal-open');

	}
});