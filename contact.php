<!doctype html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<title>Imobille Negócios</title>

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/x-icon" href="favicon.png">

		<link rel="stylesheet" href="style.css">
	</head>

	<body>
		<div id="contact">
			<?php require 'layout/header.php'; ?>

			<section class="contact-inner">
				<div class="c-1">
					<h2>Como podemos ajudá-lo?</h2>

					<hr>

					<div class="box">
						<a href="" class="box-link phone" target="_blank">
							<div class="icon-handler">
								<?php require 'icons/phone.php'; ?>
							</div>

							(47) 9 9215-1724
						</a>

						<a href="" class="box-link phone" target="_blank">
							<div class="icon-handler">
								<?php require 'icons/phone.php'; ?>
							</div>

							(47) 2125-6500
						</a>

						<a href="" class="box-link mail" target="_blank">
							<div class="icon-handler">
								<?php require 'icons/mail.php'; ?>
							</div>

							contato@imobillenegocios.com.br
						</a>

						<a href="" class="box-link location" target="_blank">
							<div class="icon-handler">
								<?php require 'icons/pin.php'; ?>
							</div>

							Rua 1500, 820 - 2401<br>Centro, Balneário Camboriú - SC, 88330-526
						</a>

						<a href="" class="box-link mail" target="_blank">
							<div class="icon-handler">
								<?php require 'icons/time.php'; ?>
							</div>

							Segunda à Domingo / 8h - 19h
						</a>
					</div>
				</div>

				<div class="c-2">
					<h2>Deixe sua mensagem!</h2>

					<hr>

					<form>
						<div class="form-row">
							<div class="form-group col-md-4">
								<input type="text" class="form-control" id="name" placeholder="Nome" required>
							</div>

							<div class="form-group col-md-4">
								<input type="email" class="form-control" id="email" placeholder="E-mail" required>
							</div>

							<div class="form-group col-md-4">
								<input type="text" class="form-control" id="phone" placeholder="(DDD) Celular" required>
							</div>
						</div>

						<div class="form-group">
							<textarea class="form-control" id="message" placeholder="Sua mensagem" required></textarea>
						</div>

						<button class="btn btn-success" type="submit">Enviar mensagem</button>
					</form>
				</div>
			</section>


			<?php require 'layout/team.php' ?>

			<?php require 'layout/order.php'; ?>

			<?php require 'layout/footer.php'; ?>
		</div>

		<script src="index.js"></script>
		<script src="main.js"></script>
	</body>
</html>