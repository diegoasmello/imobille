<!doctype html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<title>Imobille Negócios</title>

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/x-icon" href="favicon.png">

		<link rel="stylesheet" href="style.css">
	</head>

	<body>
		<div id="home">
			<?php require 'layout/header.php'; ?>

			<div class="jumbotron">
				<video autoplay muted loop id="homeVideo">
					<source src="assets/home-video.mp4" type="video/mp4">
				</video>

				<img class="background-mobile" src="assets/img/fundov_home_mobile.jpg" alt="...">

				<div class="jumbotron-inner">
					<h1 class="display-1">
						FAÇA O MELHOR<br>
						<strong>NEGÓCIO DA SUA VIDA!</strong>
					</h1>

					<p class="lead">
						Acreditamos que a qualidade de vida está relacionada a todo o contexto que envolve o lugar onde você vive.
					</p>

					<div class="jumbotron-actions">
						<a href="/" class="btn btn-home btn-secondary">
							Encomende seu imóvel
						</a>

						<a href="/" class="btn btn-home btn-primary">
							Buscar imóvel
						</a>						
					</div>

					<button data-href="#homeSearch" class="scrollTo jumbotron-mouse">
						<svg x="0px" y="0px" viewBox="0 0 18 42" style="height:50px; fill:#fff; cursor:pointer;" fill="#fff">
	                        <path d="M9,16.484c0.467,0,0.846,0.449,0.846,1.003v3.511c0,0.554-0.379,1.003-0.846,1.003c-0.467,0-0.846-0.449-0.846-1.003v-3.511C8.154,16.933,8.532,16.484,9,16.484z"></path>
	                        <rect x="7.065" y="29.875" width="3.813" height="0.868"></rect>
	                        <rect x="7.065" y="31.663" width="3.813" height="0.868"></rect>
	                        <rect x="7.065" y="33.451" width="3.813" height="1.736"></rect>
	                        <polygon points="10.864,37.537 10.864,36.107 7.111,36.107 7.111,37.537 5.271,37.537 9,42 12.729,37.537 "></polygon>
	                        <path d="M9,0C4.038,0,0,4.845,0,10.8v14.481c0,1.845,1.496,3.341,3.341,3.341h11.317c1.845,0,3.341-1.496,3.341-3.341V10.833C18,4.86,13.963,0,9,0z M16.307,24.7c0,1.286-1.043,2.329-2.329,2.329H4.021c-1.286,0-2.329-1.043-2.329-2.329V10.8c0-4.849,3.278-8.794,7.307-8.794c4.029,0,7.307,3.96,7.307,8.827V24.7z"></path>
	                    </svg>
	                </button>
				</div>

				<div class="jumbotron-footer">
					<div class="search" id="homeSearch">
						<div class="search-form">
							<div class="search-container">
								<div class="search-row">
									<div class="search-col">
										<select>
											<option>
												Todas as localizações
											</option>
										</select>
									</div>

									<div class="search-col">
										<select>
											<option>
												Todos os tipos
											</option>
											<option>
												Todos os tipos
											</option>
											<option>
												Todos os tipos
											</option>
										</select>
									</div>

									<div class="search-col">
										<select>
											<option>
												Todos os valores
											</option>
											<option>
												Todos os valores
											</option>
											<option>
												Todos os valores
											</option>
										</select>
									</div>
								</div>

								<div class="search-row collapse" id="searchCollapse">
									<div class="search-col flex-1">
										<div class="form-group">
											<label for="keywords">Busca por palavra-chave</label>
											<input type="text" class="form-control" id="keywords" placeholder="Ex: Nome do edifício">
										</div>
									</div>

									<div class="search-col">
										<div class="form-group">
											<label for="keywords">Código do imóvel</label>
											<input type="text" class="form-control" id="keywords" placeholder="Ex: Digite o código do imóvel desejado">
										</div>
									</div>
								</div>
							</div>

							<button type="submit" class="btn btn-primary btn-icon">
								<i class="material-icons">
									search
								</i>

								Buscar
							</button>
						</div>

						<button type="button" class="btn btn-link white" data-toggle="collapse" data-target="#searchCollapse">Busca avançada</button>
					</div>
				</div>
			</div>

			<section class="spotlight">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 d-flex align-items-center justify-content-center">
							<h2>Imóveis em Destaque</h2>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6 col-lg-3">
							<?php require 'layout/card-vertical.php'; ?>
						</div>

						<div class="col-md-6 col-lg-3">
							<?php require 'layout/card-vertical.php'; ?>
						</div>

						<div class="col-md-6 col-lg-3">
							<?php require 'layout/card-vertical.php'; ?>
						</div>

						<div class="col-md-6 col-lg-3">
							<?php require 'layout/card-vertical.php'; ?>
						</div>
					</div>
				</div>
			</section>

			<?php require 'layout/order.php'; ?>

			<section class="last-properties">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 d-flex align-items-center justify-content-center">
							<h2>Últimos Imóveis Cadastrados</h2>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6 col-lg-3">
							<?php require 'layout/card-vertical.php'; ?>
						</div>

						<div class="col-md-6 col-lg-3">
							<?php require 'layout/card-vertical.php'; ?>
						</div>

						<div class="col-md-6 col-lg-3">
							<?php require 'layout/card-vertical.php'; ?>
						</div>

						<div class="col-md-6 col-lg-3">
							<?php require 'layout/card-vertical.php'; ?>
						</div>
					</div>
				</div>
			</section>

			<section class="grid">
				<div class="grid-column">
					<a href="/" class="grid-cell large">
						<img src="assets/home/ibiza.jpg" alt="">

						<div class="bg"></div>

						<h3>Apartamentos Frente Mar</h3>
					</a>

					<a href="/" class="grid-cell small">
						<img src="assets/home/ibiza.jpg" alt="">

						<div class="bg"></div>

						<h3>Apartamentos Diferenciados</h3>
					</a>

					<a href="/" class="grid-cell small">
						<img src="assets/home/ibiza.jpg" alt="">

						<div class="bg"></div>

						<h3>Imóveis na Praia Brava</h3>
					</a>
				</div>

				<div class="grid-column">
					<a href="/" class="grid-cell small">
						<img src="assets/home/ibiza.jpg" alt="">

						<div class="bg"></div>

						<h3>Oportunidades de Investimento</h3>
					</a>

					<a href="/" class="grid-cell small">
						<img src="assets/home/ibiza.jpg" alt="">

						<div class="bg"></div>

						<h3>Condomínios Fechados</h3>
					</a>

					<a href="/" class="grid-cell large">
						<img src="assets/home/ibiza.jpg" alt="">

						<div class="bg"></div>

						<h3>Imóveis na Planta</h3>
					</a>
				</div>
			</section>

			<section class="about">
				<div class="container">
					<div class="row">
						<div class="col-md-10 offset-md-1 about-inner">
							<h2>A Imobille</h2>

							<p>A Imobille Negócios é uma imobiliária em Balneário Camboriú totalmente preparada para atender as expectativas de quem deseja viver neste lugar único. Através de uma cultura construída no dia a dia, baseada nas decisões, valores e visões compartilhadas entre todos os membros da equipe, nos especializamos em proporcionar o melhor negócio da vida dos nossos clientes. #viverembalneáriocamboriú</p>

							<h2>Imobille na Mídia</h2>

							<ul class="media">
								<li class="media-item">
									<a href="https://www.terra.com.br/noticias/dino/por-que-balneario-camboriu-esta-se-destacando-cada-vez-mais-em-investimentos-imobiliarios,5d3f2c5bac078c8f746db4346942fcac6utqm1gn.html" target="_blank" class="media-link">
										<img height="100" src="assets/img/logo_terra.png">
									</a>
								</li>

								<li class="media-item">
									<a href="https://www.infomoney.com.br/negocios/noticias-corporativas/noticia/7255228/conheca-projeto-alargamento-faixa-areia-praia-balneario-camboriu-que-visa" target="_blank" class="media-link">
										<img src="assets/img/logo_infomoney.png">
									</a>
								</li>

								<li class="media-item">
									<a href="https://exame.abril.com.br/negocios/dino/itajai-revela-o-maior-indice-de-valorizacao-de-imoveis-no-litoral-catarinense/" target="_blank" class="media-link">
										<img src="assets/img/logo_exame.png">
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</section>

			<?php require 'layout/team.php' ?>

			<?php require 'layout/scrollTop.php' ?>

			<?php require 'layout/footer.php'; ?>
		</div>

		<script src="index.js"></script>
		<script src="node_modules/popper.js/dist/popper.js"></script>
		<script src="main.js"></script>
	</body>
</html>