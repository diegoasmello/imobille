<section class="team">
	<div class="member">
		<img src="assets/team/4.jpg" alt="">

		<div class="member-data">
			<div class="member-name">
				Laurence Leal
			</div>

			<p class="member-info">
				Corretor de Imóveis - CRECI 27.563
				<br>
				Perito Avaliador - CNAI 22.406
			</p>

			<a href="/" target="_blank" class="member-phone">
				<span>icon</span>

				<span>(47) 9 9193-8863</span>
			</a>

			<a href="/" target="_blank" class="member-mail">
				<span>icon</span>

				<span>(47) 9 9193-8863</span>
			</a>
		</div>
	</div>

	<div class="member">
		<img src="assets/team/2.jpg" alt="">

		<div class="member-data">
			<div class="member-name">
				Laurence Leal
			</div>

			<p class="member-info">
				Corretor de Imóveis - CRECI 27.563
				<br>
				Perito Avaliador - CNAI 22.406
			</p>

			<a href="/" target="_blank" class="member-phone">
				<span>icon</span>

				<span>(47) 9 9193-8863</span>
			</a>

			<a href="/" target="_blank" class="member-mail">
				<span>icon</span>

				<span>(47) 9 9193-8863</span>
			</a>
		</div>
	</div>

	<div class="member">
		<img src="assets/team/5.jpg" alt="">

		<div class="member-data">
			<div class="member-name">
				Laurence Leal
			</div>

			<p class="member-info">
				Corretor de Imóveis - CRECI 27.563
				<br>
				Perito Avaliador - CNAI 22.406
			</p>

			<a href="/" target="_blank" class="member-phone">
				<span>icon</span>

				<span>(47) 9 9193-8863</span>
			</a>

			<a href="/" target="_blank" class="member-mail">
				<span>icon</span>

				<span>(47) 9 9193-8863</span>
			</a>
		</div>
	</div>

	<div class="member">
		<img src="assets/team/1.jpg" alt="">

		<div class="member-data">
			<div class="member-name">
				Laurence Leal
			</div>

			<p class="member-info">
				Corretor de Imóveis - CRECI 27.563
				<br>
				Perito Avaliador - CNAI 22.406
			</p>

			<a href="/" target="_blank" class="member-phone">
				<span>icon</span>

				<span>(47) 9 9193-8863</span>
			</a>

			<a href="/" target="_blank" class="member-mail">
				<span>icon</span>

				<span>(47) 9 9193-8863</span>
			</a>
		</div>
	</div>

	<div class="member">
		<img src="assets/team/6.jpg" alt="">

		<div class="member-data">
			<div class="member-name">
				Laurence Leal
			</div>

			<p class="member-info">
				Corretor de Imóveis - CRECI 27.563
				<br>
				Perito Avaliador - CNAI 22.406
			</p>

			<a href="/" target="_blank" class="member-phone">
				<span>icon</span>

				<span>(47) 9 9193-8863</span>
			</a>

			<a href="/" target="_blank" class="member-mail">
				<span>icon</span>

				<span>(47) 9 9193-8863</span>
			</a>
		</div>
	</div>

	<div class="member">
		<img src="assets/team/3.jpg" alt="">

		<div class="member-data">
			<div class="member-name">
				Laurence Leal
			</div>

			<p class="member-info">
				Corretor de Imóveis - CRECI 27.563
				<br>
				Perito Avaliador - CNAI 22.406
			</p>

			<a href="/" target="_blank" class="member-phone">
				<span>icon</span>

				<span>(47) 9 9193-8863</span>
			</a>

			<a href="/" target="_blank" class="member-mail">
				<span>icon</span>

				<span>(47) 9 9193-8863</span>
			</a>
		</div>
	</div>
</section>