<a href="interna.php" class="card card-immobile card-horizontal">
	<div id="card1-carousel" class="card-carousel carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<div class="carousel-item active">
			    <img src="assets/img/card-img-1.jpg" class="d-block" alt="">
			</div>

			<div class="carousel-item">
			    <img src="assets/img/card-img-3.jpg" class="d-block" alt="">
			</div>
		</div>

		<button class="carousel-control-prev">
			<i class="material-icons">
				keyboard_arrow_left
			</i>
	  	</button>

		<button class="carousel-control-next">
			<i class="material-icons">
				keyboard_arrow_right
			</i>
		</button>
	</div>

	<div class="code">
		#AP0807
	</div>

	<div class="card-body">
		<div class="location">
			Santa Catarina - Balneário Camboriú Camboriú
		</div>

	    <h5 class="title">
	    	Apartamento Mobiliado no Edifício Dolce Vitta Balneário Camboriú                                                                      
	    </h5>

	    <div class="table">
	    	<div class="container-fluid">
	    		<div class="row">
	    			<div class="col-md-3 cell">
	    				<div class="cell-1">
		    				<div class="cell-label">
		    					dorm.
		    				</div>

		    				<div class="cell-value">
		    					03
		    				</div>
		    			</div>

		    			<div class="cell-2 cell-dormitorio">
		    				
		    			</div>
	    			</div>

	    			<div class="col-md-3 cell">
	    				<div class="cell-1">
		    				<div class="cell-label">
		    					suítes
		    				</div>

		    				<div class="cell-value">
		    					03
		    				</div>
		    			</div>

		    			<div class="cell-2 cell-suite">
		    				
		    			</div>
	    			</div>

	    			<div class="col-md-3 cell">
	    				<div class="cell-1">
		    				<div class="cell-label">
		    					área
		    				</div>

		    				<div class="cell-value">
		    					03
		    				</div>
		    			</div>

		    			<div class="cell-2 cell-area">
		    				
		    			</div>
	    			</div>

	    			<div class="col-md-3 cell">
	    				<div class="cell-1">
		    				<div class="cell-label">
		    					vagas
		    				</div>

		    				<div class="cell-value">
		    					03
		    				</div>
		    			</div>

		    			<div class="cell-2 cell-garagem">
		    				
		    			</div>
	    			</div>
	    		</div>
	    	</div>
	    </div>

	    <p class="card-text">CENTRO MOBILIADO PRONTO PARA MORAR – Apartamento no Edifício Alzira maria Balneário Camboriú, com 135m² privativos, mobiliado e decorado, 03 dormitórios sendo…</p>

	    <div class="price">
	    	R$2.495.000
	    </div>
	</div>

	<button class="btn-whats" data-href="https://api.whatsapp.com/send?phone=5547999187474&text"></button>
</a>
