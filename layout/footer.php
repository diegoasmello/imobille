<footer id="footer">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 fixable">
				<div class="box">
					<a href="" class="box-link phone" target="_blank">
						<div class="icon-handler">
							<?php require 'icons/phone.php'; ?>
						</div>

						(47) 9 9215-1724
					</a>

					<a href="" class="box-link phone" target="_blank">
						<div class="icon-handler">
							<?php require 'icons/phone.php'; ?>
						</div>

						(47) 2125-6500
					</a>

					<a href="" class="box-link mail" target="_blank">
						<div class="icon-handler">
							<?php require 'icons/mail.php'; ?>
						</div>

						contato@imobillenegocios.com.br
					</a>

					<a href="" class="box-link location" target="_blank">
						<div class="icon-handler">
							<?php require 'icons/pin.php'; ?>
						</div>

						Rua 1500, 820 - 2401<br>Centro, Balneário Camboriú - SC, 88330-526
					</a>

					<a href="" class="box-link mail" target="_blank">
						<div class="icon-handler">
							<?php require 'icons/time.php'; ?>
						</div>

						Segunda à Domingo / 8h - 19h
					</a>
				</div>

				<p>
					A Imobille Negócios Imobiliários é uma empresa moderna de inteligência imobiliária que atua no mercado primário, com imóveis em lançamento e em construção e no mercado secundário, com imóveis prontos, novos e usados. Com sede em Balneário Camboriú, o principal campo de atuação é o mercado local e se estende à Praia Brava, em Itajaí.
				</p>
			</div>

			<div class="col-md-4 offsetable">
				<h4>Terrenos e Loteamentos</h4>

				<ul class="footer-list-1">
					<li>
						<a href="">
							Terrenos em Balneário Camboriú
						</a>
					</li>

					<li>
						<a href="">
							Terrenos Praia Brava
						</a>
					</li>

					<li>
						<a href="">
							Terrenos em Itajaí
						</a>
					</li>
				</ul>

				<h4>Apartamentos na Planta</h4>

				<ul class="footer-list-1">
					<li>
						<a href="">
							Apartamentos na Planta em Balneário Camboriú
						</a>
					</li>

					<li>
						<a href="">
							Apartamentos na Planta Praia Brava
						</a>
					</li>

					<li>
						<a href="">
							Imóveis a Preço de Custo em Balneário Camboriú
						</a>
					</li>
				</ul>

				<h4>Condomínios Fechados</h4>

				<ul class="footer-list-1">
					<li>
						<a href="">
							Condomínio Reserva Camboriú Yatch & Golf
						</a>
					</li>

					<li>
						<a href="">
							Condomínio Bella Vista Residence Club
						</a>
					</li>

					<li>
						<a href="">
							Condomínio Haras Rio do Ouro
						</a>
					</li>

					<li>
						<a href="">
							Condomínio Porto Riviera Exclusive
						</a>
					</li>

					<li>
						<a href="">
							Condomínio Vivendas do Atlântico
						</a>
					</li>

					<li>
						<a href="">
							Condomínio Villaggio da Barra
						</a>
					</li>

					<li>
						<a href="">
							Condomínios Green Ocean
						</a>
					</li>

					<li>
						<a href="">
							Condomínio Ariribá
						</a>
					</li>

					<li>
						<a href="">
							Condomínio Praia Brava
						</a>
					</li>

					<li>
						<a href="">
							Condomínio Marina Camboriú
						</a>
					</li>

					<li>
						<a href="">
							Condomínio Vila Rica
						</a>
					</li>
				</ul>

				<h4>Construtoras</h4>

				<ul class="footer-list-1">
					<li>
						<a href="">
							FG Empreendimentos
						</a>
					</li>

					<li>
						<a href="">
							Grupo Brava Beach
						</a>
					</li>

					<li>
						<a href="">
							Baggio Empreendimentos
						</a>
					</li>

					<li>
						<a href="">
							Fórmula F10 Empreendimentos
						</a>
					</li>

					<li>
						<a href="">
							Silva Packer Construtora
						</a>
					</li>

					<li>
						<a href="">
							Pasqualotto Construtora e Incorporadora
						</a>
					</li>

					<li>
						<a href="">
							AS Ramos Empreendimentos
						</a>
					</li>

					<li>
						<a href="">
							J.A Russi Construtora e Incorporadora
						</a>
					</li>

					<li>
						<a href="">
							Embraed Empreendimentos
						</a>
					</li>

					<li>
						<a href="">
							Procave Investimentos e incorporações
						</a>
					</li>

					<li>
						<a href="">
							Prediall Engenharia
						</a>
					</li>

					<li>
						<a href="">
							Ciaplan Planejamento e Construções
						</a>
					</li>

					<li>
						<a href="">
							Riviera Empreendimentos
						</a>
					</li>

					<li>
						<a href="">
							HPIO Empreendimentos
						</a>
					</li>

					<li>
						<a href="">
							RV Empreendimentos
						</a>
					</li>

					<li>
						<a href="">
							Macom Empreendimentos
						</a>
					</li>

					<li>
						<a href="">
							Pavoni e Pavoni Construtora
						</a>
					</li>

					<li>
						<a href="">
							CLN Construtora
						</a>
					</li>

					<li>
						<a href="">
							Construtora DMJ
						</a>
					</li>
				</ul>

				<h4>Casas de Luxo</h4>

				<ul class="footer-list-1">
					<li>
						<a href="">
							Casas em Balneário Camboriú
						</a>
					</li>

					<li>
						<a href="">
							Casas e Mansões de Luxo
						</a>
					</li>

					<li>
						<a href="">
							Casas em Condomínio
						</a>
					</li>

					<li>
						<a href="">
							Casas em Itajaí Praia Brava
						</a>
					</li>

					<li>
						<a href="">
							Aluguel de Casas
						</a>
					</li>
				</ul>

				<h4>Localização</h4>

				<ul class="footer-list-1">
					<li>
						<a href="">
							Apartamentos em Balneário Camboriú
						</a>
					</li>
				</ul>

				<ul class="footer-list-2">
					<li>
						<a href="">
							Frente Mar
						</a>
					</li>

					<li>
						<a href="">
							Barra Sul
						</a>
					</li>

					<li>
						<a href="">
							Barra Norte
						</a>
					</li>

					<li>
						<a href="">
							Avenida Brasil
						</a>
					</li>

					<li>
						<a href="">
							Avenida Atlântica
						</a>
					</li>

					<li>
						<a href="">
							Pioneiros
						</a>
					</li>

					<li>
						<a href="">
							Centro
						</a>
					</li>

					<li>
						<a href="">
							Quadra Mar
						</a>
					</li>
				</ul>

				<ul class="footer-list-1">
					<li>
						<a href="">
							Apartamentos em Balneário Camboriú
						</a>
					</li>
				</ul>

				<ul class="footer-list-2">
					<li>
						<a href="">
							Praia Brava
						</a>
					</li>
				</ul>


				<h4>Coberturas</h4>

				<ul class="footer-list-1">
					<li>
						<a href="">
							Coberturas em Balneário Camboriú
						</a>
					</li>

					<li>
						<a href="">
							Coberturas Praia Brava
						</a>
					</li>

					<li>
						<a href="">
							Coberturas Mobiliadas
						</a>
					</li>

					<li>
						<a href="">
							Cobertura Sem Mobília
						</a>
					</li>

					<li>
						<a href="">
							Cobertura Frente Mar
						</a>
					</li>

					<li>
						<a href="">
							Coberturas em Itajaí
						</a>
					</li>

					<li>
						<a href="">
							Cobertura Duplex
						</a>
					</li>

					<li>
						<a href="">
							Cobertura Triplex
						</a>
					</li>
				</ul>
			</div>

			<div class="col-md-4">
				<h4>Empreendimentos</h4>

				<ul class="footer-list-1">
					<li>
						<a href="">
							Edifício Four Seasons Embraed
						</a>
					</li>

					<li>
						<a href="">
							Edifício Seas Palace Residence
						</a>
					</li>

					<li>
						<a href="">
							Edifício Marina Beach Towers
						</a>
					</li>

					<li>
						<a href="">
							Edifício Privilége Residence
						</a>
					</li>

					<li>
						<a href="">
							Edifício Windsor Village
						</a>
					</li>

					<li>
						<a href="">
							Edifício Phoenix Tower
						</a>
					</li>

					<li>
						<a href="">
							Edifício Alameda Jardins
						</a>
					</li>

					<li>
						<a href="">
							Edifício Palazzo Ducalle
						</a>
					</li>

					<li>
						<a href="">
							Edifício Olympo Tower
						</a>
					</li>

					<li>
						<a href="">
							Edifício Splendia Tower
						</a>
					</li>

					<li>
						<a href="">
							Edifício Magnifique Tower
						</a>
					</li>

					<li>
						<a href="">
							Edifício Eleganza Tower
						</a>
					</li>

					<li>
						<a href="">
							Edifício Serendipity Village
						</a>
					</li>

					<li>
						<a href="">
							Edifício Solar Di Siena
						</a>
					</li>

					<li>
						<a href="">
							Edifício Esquina Bella
						</a>
					</li>

					<li>
						<a href="">
							Edifício Diamond Hill
						</a>
					</li>

					<li>
						<a href="">
							Edifício Falcon Tower
						</a>
					</li>

					<li>
						<a href="">
							Edifício Vision Tower
						</a>
					</li>

					<li>
						<a href="">
							Edifício Citta Di Vinci
						</a>
					</li>

					<li>
						<a href="">
							Edifício Renaissance
						</a>
					</li>

					<li>
						<a href="">
							Edifício Le Majestic
						</a>
					</li>

					<li>
						<a href="">
							Edifício Notting Hill
						</a>
					</li>

					<li>
						<a href="">
							Edifício North Shore
						</a>
					</li>

					<li>
						<a href="">
							Edifício Alexandria
						</a>
					</li>

					<li>
						<a href="">
							Edifício Metropolis
						</a>
					</li>

					<li>
						<a href="">
							Edifício One Tower
						</a>
					</li>

					<li>
						<a href="">
							Edifício Quintas do Arpoador
						</a>
					</li>

					<li>
						<a href="">
							Edifício Sky Tower
						</a>
					</li>

					<li>
						<a href="">
							Edifício Avangard Exclusive House
						</a>
					</li>

					<li>
						<a href="">
							Edifício Le Portier
						</a>
					</li>

					<li>
						<a href="">
							Edifício Grand Royale
						</a>
					</li>

					<li>
						<a href="">
							Edifício Apogee
						</a>
					</li>

					<li>
						<a href="">
							Edifício Le Parc
						</a>
					</li>

					<li>
						<a href="">
							Edifício Dalcelis
						</a>
					</li>

					<li>
						<a href="">
							Edifício Royalton
						</a>
					</li>

					<li>
						<a href="">
							Edifício Royal Tower
						</a>
					</li>

					<li>
						<a href="">
							Edifício Holambra
						</a>
					</li>

					<li>
						<a href="">
							Le Visage Divinité
						</a>
					</li>

					<li>
						<a href="">
							Garden Village
						</a>
					</li>

					<li>
						<a href="">
							Ville Del Acqua
						</a>
					</li>

					<li>
						<a href="">
							Pacoste Home
						</a>
					</li>

					<li>
						<a href="">
							Edifício Pharos
						</a>
					</li>

					<li>
						<a href="">
							Edifício Acqua
						</a>
					</li>

					<li>
						<a href="">
							Edifício Parque das Nações
						</a>
					</li>

					<li>
						<a href="">
							Solar Gonçalves Residence
						</a>
					</li>

					<li>
						<a href="">
							Moradas Ilhas Marianas Embraed
						</a>
					</li>

					<li>
						<a href="">
							Hampton’s Village Embraed
						</a>
					</li>

					<li>
						<a href="">
							Edifício Aurora Embraed
						</a>
					</li>

					<li>
						<a href="">
							Edifício Illuminati
						</a>
					</li>

					<li>
						<a href="">
							Edifício Acqualina Residence
						</a>
					</li>

					<li>
						<a href="">
							Edifício Skyline Tower
						</a>
					</li>

					<li>
						<a href="">
							Edifício Boreal Tower
							</a>
					</li>

					<li>
						<a href="">
							Magique Home Living Embraed
							</a>
					</li>

					<li>
						<a href="">
							Embraed Tower
						</a>
					</li>

					<li>
						<a href="">
							Edifício Império das Ondas
						</a>
					</li>

					<li>
						<a href="">
							Edifício Dolce Vitta
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</footer>