<div id="modalContact" class="modal modal-contact" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <div class="modal-body">
                <div class="phone-handler">
                    <a href="" class="phone">
                        <?php require 'icons/whats.php'; ?>

                        (47) 99999-9999
                    </a>

                    <button class="btn btn-link white btn-show-phone">
                        Ver telefone
                    </button>
                </div>

                <div class="label">
                    Saiba mais sobre este imóvel
                </div>

                <form>
                    <input type="text" class="form-control" placeholder="Nome" required>

                    <input type="email" class="form-control" placeholder="E-mail" required>

                    <input type="text" class="form-control" placeholder="(DDD) Telefone" required>

                    <textarea class="form-control">Olá, tenho interesse neste imóvel: Apartamento no edifício Vitra Balneário Camboriú | código #AP0999, preço a partir de R$ 2.650.000. Aguardo o contato. Obrigado.</textarea>

                    <button type="submit" class="btn btn-success btn-submit">
                        Receber informações
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>