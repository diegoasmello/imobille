<header id="header">
	<nav class="navbar navbar-expand-lg">
		<a class="navbar-brand" href="/">
			<img  src="assets/logo.svg">
		</a>

		<ul class="navbar-nav" id="headerNav">
			<li class="nav-item dropdown">
				<button class="nav-link dropdown-toggle">Imóveis</button>

				<div class="dropdown-menu">
					<div class="dropdown-inner">
						<a class="dropdown-item" href="list.php">Todos os imóveis</a>
						<a class="dropdown-item" href="list.php">Imóveis com tour 360º</a>
						<a class="dropdown-item" href="list.php">Lançamentos</a>
						<a class="dropdown-item" href="list.php">Casas em Balneário Camboriú</a>
						<a class="dropdown-item" href="list.php">Imóveis abaixo do preço</a>
						<a class="dropdown-item" href="list.php">Aluguel de casas</a>
					</div>
				</div>
			</li>

			<li class="nav-item dropdown">
				<button class="nav-link dropdown-toggle">Apartamentos</button>

				<div class="dropdown-menu">
					<div class="dropdown-inner">
						<a class="dropdown-item" href="list.php">Todos os imóveis</a>
						<a class="dropdown-item" href="list.php">Imóveis com tour 360º</a>
						<a class="dropdown-item" href="list.php">Lançamentos</a>
						<a class="dropdown-item" href="list.php">Casas em Balneário Camboriú</a>
						<a class="dropdown-item" href="list.php">Imóveis abaixo do preço</a>
						<a class="dropdown-item" href="list.php">Aluguel de casas</a>
					</div>
				</div>
			</li>

			<li class="nav-item dropdown">
				<button class="nav-link dropdown-toggle">Empreendimentos</button>

				<div class="dropdown-menu">
					<div class="dropdown-inner">
						<a class="dropdown-item" href="list.php">Todos os imóveis</a>
						<a class="dropdown-item" href="list.php">Imóveis com tour 360º</a>
						<a class="dropdown-item" href="list.php">Lançamentos</a>
						<a class="dropdown-item" href="list.php">Casas em Balneário Camboriú</a>
						<a class="dropdown-item" href="list.php">Imóveis abaixo do preço</a>
						<a class="dropdown-item" href="list.php">Aluguel de casas</a>
					</div>
				</div>
			</li>

			<li class="nav-item dropdown">
				<button class="nav-link dropdown-toggle">Praia Brava</button>

				<div class="dropdown-menu">
					<div class="dropdown-inner">
						<a class="dropdown-item" href="list.php">Todos os imóveis</a>
						<a class="dropdown-item" href="list.php">Imóveis com tour 360º</a>
						<a class="dropdown-item" href="list.php">Lançamentos</a>
						<a class="dropdown-item" href="list.php">Casas em Balneário Camboriú</a>
						<a class="dropdown-item" href="list.php">Imóveis abaixo do preço</a>
						<a class="dropdown-item" href="list.php">Aluguel de casas</a>
					</div>
				</div>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="list.php">Terrenos</a>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="list.php">Blog</a>
			</li>

			<li class="nav-item">
				<a class="nav-link highlight" href="contact.php">Contato</a>
			</li>

			<li class="nav-item">
				<a class="nav-link highlight" target="_blank" href="style-guide.php">Guia de estilo</a>
			</li>
		</ul>

		<button class="d-lg-none btn-menu-toggle" id="btnHeaderToggle">
			<i class="material-icons">
			menu
			</i>
		</button>
	</nav>
</header>