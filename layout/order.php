<section id="order">
	<div class="video-wrapper">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/XYUl1vlgnTo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</div>

	<div class="order-inner">
		<h2 class="order-title display-2">Simplifique sua busca, encomende!</h2>

		<p>O jeito mais fácil de encontrar o seu imóvel em Balneário Camboriú e Praia Brava.</p>

		<div id="ratingCarousel" class="carousel slide" data-touch="true" data-interval="2000" data-ride="carousel">
			<div class="carousel-inner">
		    	<div class="carousel-item active">
		        	<div class="">
						<p>
							Foi um prazer fazer negócio com a Imobille. Empresa honesta, realizou o meu negócio com agilidade e foi adequada para todas as partes. E mesmo eu não estando no país, conseguimos fechar negócio.
						</p>

						<div class="author">
							Nicolas K.
						</div>
					</div>
		    	</div>

		    	<div class="carousel-item">
		      		<div class="">
						<p>
							Sou cliente da Imobille e agradeço pelo seu atendimento dinâmico e transparente. Profissionalismo e agilidade me parecem ser os valores da empresa. Parabéns pelo excelente trabalho.
						</p>

						<div class="author">
							Viviane P.
						</div>
					</div>
		    	</div>
		  	</div>

		  	<ol class="carousel-indicators">
			    <li data-target="#ratingCarousel" data-slide-to="0" class="active"></li>
			    <li data-target="#ratingCarousel" data-slide-to="1"></li>
		  	</ol>
		</div>

		 <a href="/" class="btn btn-primary">Encontre seu imóvel</a>
	</div>
</section>
