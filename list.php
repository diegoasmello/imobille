<!doctype html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<title>Imobille Negócios</title>

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/x-icon" href="favicon.png">

		<link rel="stylesheet" href="style.css">
	</head>

	<body>
		<div id="list">
			<?php require 'layout/header.php'; ?>

			<main>
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 col-lg-3 filter-handler">
							<div class="close d-flex d-lg-none">
								<i class="material-icons">
									close
								</i>
							</div>

							<div class="filter">
								<div class="filter-main">
									<div class="filter-main-row">
										<label for="">Localização do imóvel</label>

										<select>
											<option>Todas as localizações</option>
											<option>Balneário Camboriú</option>
											<option>Itajaí/Praia Brava</option>
										</select>
									</div>

									<div class="filter-main-row">
										<label for="">Valor do imóvel</label>

										<select>
											<option>Todos os valores</option>
										</select>
									</div>

									<div class="filter-main-row">
										<label for="">Dormitórios</label>

										<div class="filter-btn-group">
											<button class="filter-btn">
												1
											</button>

											<button class="filter-btn">
												2
											</button>

											<button class="filter-btn">
												3
											</button>

											<button class="filter-btn">
												4
											</button>

											<button class="filter-btn">
												5+
											</button>
										</div>
									</div>

									<div class="filter-main-row">
										<label for="">Vagas de garagem</label>

										<div class="filter-btn-group">
											<button class="filter-btn">
												1
											</button>

											<button class="filter-btn">
												2
											</button>

											<button class="filter-btn">
												3
											</button>

											<button class="filter-btn">
												4
											</button>

											<button class="filter-btn">
												5+
											</button>
										</div>
									</div>
								</div>

								<div class="filter-item">
									<div class="filter-item-title">
										Características do Imóvel
									</div>

									<div class="filter-item-body">
										<div class="form-check">
											<input class="form-check-input" id="check1" type="checkbox">
											<label class="form-check-label" for="check1">
												Churrasqueira
											</label>
										</div>

										<div class="form-check">
											<input class="form-check-input" id="check2" type="checkbox">
											<label class="form-check-label" for="check2">
												Mobiliado
											</label>
										</div>

										<div class="form-check">
											<input class="form-check-input" id="check3" type="checkbox">
											<label class="form-check-label" for="check3">
												Pé Direito Duplo
											</label>
										</div>

										<div class="form-check">
											<input class="form-check-input" id="check4" type="checkbox">
											<label class="form-check-label" for="check4">
												Vista Mar
											</label>
										</div>

										<div class="form-check">
											<input class="form-check-input" id="check5" type="checkbox">
											<label class="form-check-label" for="check5">
												Sacada
											</label>
										</div>
									</div>
								</div>

								<div class="filter-item">
									<div class="filter-item-title">
										Características do Edifício
									</div>

									<div class="filter-item-body">
										<div class="filter-check-group">
											<input type="checkbox">

											<label>Churrasqueira</label>
										</div>

										<div class="filter-check-group">
											<input type="checkbox">

											<label>Mobiliado</label>
										</div>

										<div class="filter-check-group">
											<input type="checkbox">

											<label>Pé Direito Duplo</label>
										</div>

										<div class="filter-check-group">
											<input type="checkbox">

											<label>Vista Mar</label>
										</div>

										<div class="filter-check-group">
											<input type="checkbox">

											<label>Sacada</label>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-12 col-lg-9 results-handler">
							<div class="results-label">
								<strong>84</strong> resultados:
							</div>

							<div class="filters-applied">
								<ul>
									<li class="active">
										<div class="filter-name">
											02 dorms.
										</div>

										<button class="close">
											<i class="material-icons">
											close
											</i>
										</button>
									</li>

									<li class="active">
										<div class="filter-name">
											02 dorms.
										</div>

										<button class="close">
											<i class="material-icons">
											close
											</i>
										</button>
									</li>
								</ul>
							</div>

							<div class="list">
								<?php require 'layout/card-horizontal.php'; ?>

								<?php require 'layout/card-horizontal.php'; ?>

								<?php require 'layout/card-horizontal.php'; ?>
							</div>

							<div class="find">
								<button class="btn btn-primary btn-two-line btn-related">
									<div class="line-1">Ainda não achou?</div>

									<div class="line-2">Clique aqui, encontramos pra você!</div>
								</button>
							</div>

							<div class="list">
								<?php require 'layout/card-horizontal.php'; ?>

								<?php require 'layout/card-horizontal.php'; ?>

								<?php require 'layout/card-horizontal.php'; ?>
							</div>

							<nav class="pagination-container">
							  	<ul class="pagination">
								    <li class="page-item active"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li><li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li><li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
							  	</ul>
							</nav>
						</div>
					</div>
				</div>
			</main>

			<nav class="list-menu">
				<button class="btn btn-filter-toggle">
					<div class="not-active">
						<?php require 'icons/filter.php'; ?>
					</div>

					<div class="active">
						<i class="material-icons">
							close
						</i>
					</div>
				</button>

				<button data-href="" class="btn btn-whats">
					Contato via WhatsApp
				</button>

				<button class="btn btn-email" data-toggle="modal" data-target="#modalContact">
					<?php require 'icons/mail.php'; ?>
				</button>
			</nav>

			<?php require 'layout/order.php'; ?>

			<?php require 'layout/modal-contact.php' ?>

			<?php require 'layout/scrollTop.php' ?>

			<?php require 'layout/footer.php'; ?>
		</div>

		<script src="index.js"></script>
		<script src="node_modules/popper.js/dist/popper.js"></script>
		<script src="main.js"></script>
	</body>
</html>